import gzip
import json
import httpretty

from myinfinitycli import api


FAKECMDHASH = '1234567890abcdef1234567890abcdef'


def test_login():
    baseurl = 'https://baseurl'
    vendor = 'vendor'

    def callback(request, uri, response_headers):
        assert uri == api.LOGIN_URL % (baseurl, vendor)
        assert len(request.parsed_body['m_cAction']) == 1
        assert request.parsed_body['m_cAction'][0] == 'login'
        assert len(request.parsed_body['m_cUserName']) == 1
        assert request.parsed_body['m_cUserName'][0] == 'usern'
        assert len(request.parsed_body['m_cPassword']) == 1
        assert request.parsed_body['m_cPassword'][0] == 'password'
        response_headers['Set-Cookie'] = 'JSESSIONID=fakesession'
        return [200, response_headers, '']

    httpretty.enable(allow_net_connect=False)
    httpretty.register_uri(httpretty.POST, api.LOGIN_URL % (baseurl, vendor), body=callback)
    assert api.login(baseurl, vendor, 'usern', 'password') == 'fakesession'


def test_logout():
    baseurl = 'https://baseurl'
    vendor = 'vendor'

    def callback(request, uri, response_headers):
        assert uri == api.LOGOUT_URL % (baseurl, vendor)
        assert request.headers['Cookie'] == 'JSESSIONID=fakesession'
        return [200, response_headers, '']

    httpretty.enable(allow_net_connect=False)
    httpretty.register_uri(httpretty.GET, api.LOGOUT_URL % (baseurl, vendor), body=callback)
    api.logout(baseurl, vendor, 'fakesession')


def test_get_pdf_list():
    baseurl = 'https://baseurl'
    vendor = 'vendor'

    def callback_cmdhash(request, uri, response_headers):
        assert uri == api.GET_CMDHASH_URL % (baseurl, vendor)
        assert request.headers['Cookie'] == 'JSESSIONID=fakesession'
        cmd = 'ushp_qfolderemsel_noread "%s"' % FAKECMDHASH
        return [200, response_headers, cmd]

    def callback(request, uri, response_headers):
        assert uri == api.LIST_PDF_URL % (baseurl, vendor)
        assert request.headers['Cookie'] == 'JSESSIONID=fakesession'
        assert len(request.parsed_body['rows']) == 1
        assert request.parsed_body['rows'][0] == '10'
        assert len(request.parsed_body['startrow']) == 1
        assert request.parsed_body['startrow'][0] == '0'
        assert len(request.parsed_body['count']) == 1
        assert request.parsed_body['count'][0] == 'false'
        assert len(request.parsed_body['sqlcmd']) == 1
        assert request.parsed_body['sqlcmd'][0] == 'ushp_qfolderemsel_noread'
        assert len(request.parsed_body['orderby']) == 1
        assert request.parsed_body['orderby'][0] == 'chkread, dtstartview desc, LOGICFOLDERPATH'
        assert len(request.parsed_body['pFLVIEWNG']) == 1
        assert request.parsed_body['pFLVIEWNG'][0] == 'S'
        assert len(request.parsed_body['cmdhash']) == 1
        assert request.parsed_body['cmdhash'][0] == FAKECMDHASH

        return [200, response_headers, json.dumps({'Fields': ['f1', 'f2', 'f3'], 'Data': [['r1f1', 'r1f2', 'r1f3'], ['r2f1', 'r2f2', 'r2f3']]})]

    httpretty.enable(allow_net_connect=False)
    httpretty.register_uri(httpretty.GET, api.GET_CMDHASH_URL % (baseurl, vendor), body=callback_cmdhash)
    httpretty.register_uri(httpretty.POST, api.LIST_PDF_URL % (baseurl, vendor), body=callback)
    response = api.get_pdf_list(baseurl, vendor, 'fakesession', 10)
    assert len(response) == 2
    for i, pdfinfos in enumerate(response):
        assert len(pdfinfos.keys()) == 3
        rownum = i + 1
        assert pdfinfos['f1'] == 'r%df1' % rownum
        assert pdfinfos['f2'] == 'r%df2' % rownum
        assert pdfinfos['f3'] == 'r%df3' % rownum


def test_get_pdf():
    baseurl = 'https://baseurl'
    vendor = 'vendor'

    def callback(request, uri, response_headers):
        assert uri == api.DOWNLOAD_PDF_URL % (baseurl, vendor)
        assert request.headers['Cookie'] == 'JSESSIONID=fakesession'
        assert len(request.parsed_body['idfolder']) == 1
        assert request.parsed_body['idfolder'][0] == 'fakepdfid'
        return [200, response_headers, b'fakepdfresponse']

    httpretty.enable(allow_net_connect=False)
    httpretty.register_uri(httpretty.POST, api.DOWNLOAD_PDF_URL % (baseurl, vendor), body=callback)
    resp = api.get_pdf(baseurl, vendor, 'fakesession', 'fakepdfid')
    data = resp.read()

    assert data.decode() == u'fakepdfresponse'
