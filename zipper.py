import os
import stat
import zipfile

MAIN = '''# -*- coding: utf-8 -*-
import myinfinitycli.cli
myinfinitycli.cli.main()
'''

OUTPUT_NAME = 'dist/myinfinitycli.pyz'


def main():
    with open(OUTPUT_NAME, 'wb') as f:
        f.write(b'#!/usr/bin/env python\n')
        with zipfile.ZipFile(f, 'w', zipfile.ZIP_DEFLATED) as z:
            z.write('myinfinitycli/api.py', 'myinfinitycli/api.py')
            z.write('myinfinitycli/cli.py', 'myinfinitycli/cli.py')
            z.write('myinfinitycli/facade.py', 'myinfinitycli/facade.py')
            z.write('myinfinitycli/__init__.py', 'myinfinitycli/__init__.py')
            z.writestr('__main__.py', MAIN.encode('utf-8'))
    os.chmod(OUTPUT_NAME, os.stat(OUTPUT_NAME).st_mode | stat.S_IEXEC)


if __name__ == '__main__':
    main()
