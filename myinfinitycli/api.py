import json
import re

try:
    # For Python 3.0 and later
    import urllib.request as urllib2
except ImportError:
    # Fall back to Python 2's urllib2
    import urllib2

try:
    import http.cookiejar as cookielib
except ImportError:
    import cookielib

try:
    # For Python 3.0 and later
    from urllib.parse import urlencode
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib import urlencode

HOSTNAME = '%s'
BASEPATH = '%s/servlet'
JSP_BASEPATH = '%s/jsp'

LOGIN_ENDPOINT = 'cp_login'
LIST_PDF_ENDPOINT = 'SQLDataProviderServer'
DOWNLOAD_PDF_ENDPOINT = 'ushp_bexecdoc'
LOGOUT_ENDPOINT = 'usut_blogout'

GET_CMDHASH_ENDPOINT = 'ushp_one_column_model.jsp'

LOGIN_URL = '%s/%s/%s' % (HOSTNAME, BASEPATH, LOGIN_ENDPOINT)
LIST_PDF_URL = '%s/%s/%s' % (HOSTNAME, BASEPATH, LIST_PDF_ENDPOINT)
DOWNLOAD_PDF_URL = '%s/%s/%s' % (HOSTNAME, BASEPATH, DOWNLOAD_PDF_ENDPOINT)
LOGOUT_URL = '%s/%s/%s' % (HOSTNAME, BASEPATH, LOGOUT_ENDPOINT)

GET_CMDHASH_URL = '%s/%s/%s' % (HOSTNAME, JSP_BASEPATH, GET_CMDHASH_ENDPOINT)

CMDHASH_REGEX = re.compile('"([0-9a-f]{32})"')


def login(baseurl, vendor, username, password):
    login_url = LOGIN_URL % (baseurl, vendor)
    data = urlencode({'m_cUserName': username, 'm_cPassword': password, 'm_cAction': 'login'}).encode()
    cj = cookielib.CookieJar()
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
    login_response = opener.open(urllib2.Request(login_url, data=data))
    for cookie in cj:
        if cookie.name == 'JSESSIONID':
            return cookie.value


def logout(baseurl, vendor, session_cookie):
    logout_url = LOGOUT_URL % (baseurl, vendor)
    headers = {'Cookie': 'JSESSIONID=%s' % session_cookie}
    urllib2.urlopen(urllib2.Request(logout_url, headers=headers))


def _get_cmdhash(baseurl, vendor, session_cookie, cmd):
    get_cmdhash_url = GET_CMDHASH_URL % (baseurl, vendor)
    headers = {
        'Cookie': 'JSESSIONID=%s' % session_cookie,
    }
    response = urllib2.urlopen(urllib2.Request(get_cmdhash_url, headers=headers))
    for line in response:
        strline = str(line)
        if cmd in strline:
            match = CMDHASH_REGEX.search(strline)
            if match:
                return match.group(1)


def get_pdf_list(baseurl, vendor, session_cookie, rows):
    list_pdf_url = LIST_PDF_URL % (baseurl, vendor)
    headers = {'Cookie': 'JSESSIONID=%s' % session_cookie}
    cmd = 'ushp_qfolderemsel_noread'
    data = urlencode({
        'rows': str(rows),
        'startrow': '0',
        'count': 'false',
        'sqlcmd': cmd,
        'orderby': 'chkread, dtstartview desc, LOGICFOLDERPATH',
        'pFLVIEWNG': 'S',
        'cmdhash': _get_cmdhash(baseurl, vendor, session_cookie, cmd)
    }).encode()
    response = urllib2.urlopen(urllib2.Request(list_pdf_url, data=data, headers=headers))
    json_response = json.loads(response.read().decode('utf-8'))
    response_fields = json_response['Fields']
    return [dict(zip(response_fields, data_item)) for data_item in json_response['Data'] if isinstance(data_item, list)]


def get_pdf(baseurl, vendor, session_cookie, idpdf):
    download_pdf_url = DOWNLOAD_PDF_URL % (baseurl, vendor)
    headers = {'Cookie': 'JSESSIONID=%s' % session_cookie}
    data = urlencode({'idfolder': idpdf}).encode()
    return urllib2.urlopen(urllib2.Request(download_pdf_url, data=data, headers=headers))
