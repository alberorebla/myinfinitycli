import os
import re

from myinfinitycli.api import get_pdf_list, get_pdf, login, logout, HOSTNAME

MONTHYEAR_REGEX = re.compile(r'^.* ([0-9]{2})-([0-9]{4}) .*$')
CU_REGEX = re.compile(r'Certificazione Unica redditi ([0-9]{4}) .*')


def _get_payroll_list(baseurl, vendor, jse, count):
    out_list = []
    for pdf in get_pdf_list(baseurl, vendor, jse, count):
        monthyear = MONTHYEAR_REGEX.search(pdf['dsfolder'])
        if monthyear:
            out_list.append({
                'id': pdf['idfolder'],
                'output_name': '%s-%s_payroll_%s.pdf' % (monthyear.group(2), monthyear.group(1), pdf['idfolder']),
                'pub_date': pdf['dtpubb'],
                'refperiod': (monthyear.group(2), monthyear.group(1))
            })
        else:
            cud = CU_REGEX.search(pdf['dsfolder'])
            if cud:
                out_list.append({
                    'id': pdf['idfolder'],
                    'output_name': '%s-00_CUD_%s.pdf' % (cud.group(1), pdf['idfolder']),
                    'pub_date': pdf['dtpubb'],
                    'refperiod': (cud.group(1), '00')
                })
    return out_list


def _write_to_file_if_needed(baseurl, vendor, jse, pdf, output_folder):
    output_name = os.path.join(output_folder, pdf.get('output_name'))
    if not os.path.exists(output_name):
        with open(output_name, 'wb') as out_file:
            pdf_data = get_pdf(baseurl, vendor, jse, pdf.get('id'))
            for data in pdf_data:
                out_file.write(data)


def _download_payroll(baseurl, vendor, jse, counter, output_folder, payroll_date=None):
    for pdf in _get_payroll_list(baseurl, vendor, jse, counter):
        if payroll_date is None or '-'.join(pdf.get('refperiod')) == payroll_date:
            _write_to_file_if_needed(baseurl, vendor, jse, pdf, output_folder)


class MyInfinityFacade(object):

    def __init__(self, baseurl, vendor, user, passw):
        self.baseurl = baseurl
        self.vendor = vendor
        self.user = user
        self.passw = passw
        self.j_session_cookie = None

    def __enter__(self):
        self.j_session_cookie = login(self.baseurl, self.vendor, self.user, self.passw)
        return self

    def __exit__(self, *args):
        logout(self.baseurl, self.vendor, self.j_session_cookie)

    def get_payroll_list(self, count):
        return _get_payroll_list(self.baseurl, self.vendor, self.j_session_cookie, count)

    def download_last_payroll(self, count, output_folder):
        return _download_payroll(self.baseurl, self.vendor, self.j_session_cookie, count, output_folder)

    def download_specific_payroll(self, payroll_date, output_folder):
        return _download_payroll(self.baseurl, self.vendor, self.j_session_cookie, 10, output_folder, payroll_date)
