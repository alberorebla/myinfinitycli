# ![Logo](/img/the-infinity-gauntlet64.png) myinfinity PyCLI

myinfinitycli is a Python CLI application 

## Basic setup

Install the requirements:
```
$ pip install -r requirements.txt
```

Run the application:
```
$ python myinfinitycli.py --help
```

The application will prompt for credentials if not provided by command line options or .netrc integration.

To use .netrc integration, create an entry in ~/.netrc file as follows:
```
machine https://www.myinfinityportal.it
    login USERNAME
    account VENDOR
    password PASSWORD
```

To override .netrc account credentials pass command line options:
```
$ python myinfinitycli.py list --vendor VENDOR --user $USER --passw $PASS
```

Get the list of last the 10 payroll:
```
$ python myinfinitycli.py list
```

Get the list of last the N payroll:
```
$ python myinfinitycli.py list --count N
```

Download the latest payroll:
```
$ python myinfinitycli.py download-last --output-folder ~/Downloads/
```

Download the latest N payroll:
```
$ python myinfinitycli.py download-last --count N --output-folder ~/Downloads/
```

Download the specific payroll:
```
$ python myinfinitycli.py download --payroll-date 2018-10 --output-folder ~/Downloads/
```


To run the tests:
```
    $ pytest
```







<div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
