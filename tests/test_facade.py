import unittest
import myinfinitycli.facade
from myinfinitycli.facade import _get_payroll_list

GET_PAYROLL_LIST = [{u'idfolder': u'aaazamzufn', u'dtpubb': u'2018-10-31 16:03:11.0', u'dsfolder': u'Libro unico di 10-2018  (XXXXXX00X00X000X-001)'},
{u'idfolder': u'bbbzamzufn', u'dtpubb': u'2018-11-31 16:03:11.0', u'dsfolder': u'Libro unico di 11-2018  (XXXXXX00X00X000X-001)'},
{u'idfolder': u'ccczamzufn', u'dtpubb': u'2018-12-31 16:03:11.0', u'dsfolder': u'Libro unico di 12-2018  (XXXXXX00X00X000X-001)'},]


class TestFacade(unittest.TestCase):

    def setUp(self):
        myinfinitycli.facade.get_pdf_list = lambda u, a, b, c : GET_PAYROLL_LIST[0:c]

    def test_facade_get_N_payroll_list(self):
        self.assertEqual(1, len(_get_payroll_list('baseurl', 'fake-vendor', 'fake-session-id', 1)))
        self.assertEqual(2, len(_get_payroll_list('baseurl', 'fake-vendor', 'fake-session-id', 2)))
        self.assertEqual(3, len(_get_payroll_list('baseurl', 'fake-vendor', 'fake-session-id', 3)))


if __name__ == '__main__':
    unittest.main()
