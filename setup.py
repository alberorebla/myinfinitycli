import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="myinfinitycli",
    version="0.0.2",
    author="alberorebla",
    description="My Infinity Portal CLI",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/alberorebla/myinfinitycli",
    packages=setuptools.find_packages(exclude=['tests']),
    classifiers=[
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        'console_scripts': ['myinfinitycli=myinfinitycli.cli:main'],
    },
    install_requires=[
    ]
)
