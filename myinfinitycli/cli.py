#!/usr/bin/env python
import argparse
import getpass
import netrc

from myinfinitycli.facade import MyInfinityFacade

try:
    input = raw_input
except NameError:
    pass

def _get_prompt_credentials(baseurl):
    vendor = input('Please ender a vendor: ')
    user = input('Please enter a username: ')
    passw = getpass.getpass('Please enter a password: ')
    return (baseurl, vendor, user, passw)


def _get_credentials(baseurl, vendor, user, passw):
    if vendor and user and passw:
        return (baseurl, vendor, user, passw)
    else:
        try:
            auth_tuple = netrc.netrc().authenticators(baseurl)
            if auth_tuple:
                return (baseurl, auth_tuple[1], auth_tuple[0], auth_tuple[2])
            else:
                return _get_prompt_credentials(baseurl)
        except IOError:
            return _get_prompt_credentials(baseurl)


def cli_get_list_of_payroll(baseurl, vendor, user, passw, count, **kwargs):
    with MyInfinityFacade(*_get_credentials(baseurl, vendor, user, passw)) as mif:
        print('|__ Pubblication date___|_______________file_name______________')
        for pdf in mif.get_payroll_list(count):
            print('| %s | %s' % (pdf.get('pub_date'), pdf.get('output_name')))


def cli_download_last_payroll(baseurl, vendor, user, passw, count, output_folder, **kwargs):
    with MyInfinityFacade(*_get_credentials(baseurl, vendor, user, passw)) as mif:
        mif.download_last_payroll(count, output_folder)


def cli_download_specific_payroll(baseurl, vendor, user, passw, payroll_date, output_folder, **kwargs):
    with MyInfinityFacade(*_get_credentials(baseurl, vendor, user, passw)) as mif:
        mif.download_specific_payroll(payroll_date, output_folder)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('baseurl', help='Base URL')
    parser.add_argument('--vendor', '-v', help='Myinfinityportal vendor')
    parser.add_argument('--user', '-u', help='Myinfinityportal user')
    parser.add_argument('--passw', '-p', help='Myinfinityportal password')
    subparsers = parser.add_subparsers(dest='cmd')
    subparsers.required = True
    parser_list = subparsers.add_parser('list') 
    parser_list.add_argument('--count', '-c', default=10, help='Number of rows, default 10')
    parser_list.set_defaults(func=cli_get_list_of_payroll)

    parser_download_last = subparsers.add_parser('download-last')
    parser_download_last.add_argument('--count', '-c', default=1, help='Number of payrolls to download, default 1')
    parser_download_last.add_argument('--output-folder', '-o', required=True, help='Download destination folder')
    parser_download_last.set_defaults(func=cli_download_last_payroll)
    
    parser_download = subparsers.add_parser('download')
    parser_download.add_argument('--payroll-date', '-d', required=True, help='Payroll date to download in YYYY-MM format (e.g. 2018-10)')
    parser_download.add_argument('--output-folder', '-o', required=True, help='Download destination folder')
    parser_download.set_defaults(func=cli_download_specific_payroll)
    res = parser.parse_args()
    res.func(**vars(res))


if __name__ == '__main__':
    main()
